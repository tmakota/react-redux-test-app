/**
 * Created by taras on 29.05.16.
 */

import React, {PropTypes} from 'react';
import {Table, TableBody, TableHeader, TableHeaderColumn, TableRow, TableRowColumn} from 'material-ui/Table';
import FlatButton from 'material-ui/FlatButton';
import {Link} from 'react-router';


const TablePage = ({items, remove}) => (
    <Table selectable={false}>
        <TableHeader displaySelectAll={false} adjustForCheckbox={false}>
            <TableRow>
                <TableHeaderColumn>Name</TableHeaderColumn>
                <TableHeaderColumn>{" "}</TableHeaderColumn>
                <TableHeaderColumn>{" "}</TableHeaderColumn>
                <TableHeaderColumn>{" "}</TableHeaderColumn>
            </TableRow>
        </TableHeader>
        <TableBody displayRowCheckbox={false}>
            {items.map((item,index) => {
                return (
                    <TableRow key={item.get('id')}>
                        <TableRowColumn>{item.get('title')}</TableRowColumn>
                        <TableRowColumn>
                            <Link to={'/read/' + item.get('id')}><FlatButton label="Read"/></Link>
                        </TableRowColumn>
                        <TableRowColumn>
                            <Link to={'/edit/' + item.get('id')}><FlatButton label="Edit" primary={true}/></Link>
                        </TableRowColumn>
                        <TableRowColumn>
                            <button onClick={remove} id={index}>delete</button>
                        </TableRowColumn>
                    </TableRow>
                )
                }
            )}
        </TableBody>
    </Table>
);

TablePage.propTypes = {
    items: PropTypes.object.isRequired,
    remove: PropTypes.func.isRequired
};

export default TablePage;