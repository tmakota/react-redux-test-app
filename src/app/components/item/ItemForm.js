/**
 * Created by taras on 29.05.16.
 */

import React from 'react';
import {deepOrange500} from 'material-ui/styles/colors';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';

import TextInput from '../common/TextInput';

const muiTheme = getMuiTheme({
    palette: {
        accent1Color: deepOrange500
    }
});

const ItemForm = ({item, onChange}) => {
    return (
        <MuiThemeProvider muiTheme={muiTheme}>
            <form>
                <h1>Edit item</h1>
                <TextInput
                    name="title"
                    label="Title"
                    value={item.get('title')}
                    onChange={onChange}/>

                <TextInput
                    name="status"
                    label="Status"
                    value={item.get('status')}
                    onChange={onChange}/>

                <TextInput
                    name="date"
                    label="Date"
                    value={item.get('date')}
                    onChange={onChange}/>

            </form>
        </MuiThemeProvider>
    );
};

ItemForm.propTypes = {
    item: React.PropTypes.object.isRequired,
    onChange: React.PropTypes.func.isRequired
};

export default ItemForm;
