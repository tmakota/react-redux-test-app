/**
 * Created by taras on 29.05.16.
 */

import React, {PropTypes} from 'react';
import {connect} from 'react-redux';
import {deepOrange500} from 'material-ui/styles/colors';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import ItemData from './ItemData';
import FloatingActionButton from 'material-ui/FloatingActionButton';
import HomeLink from 'material-ui/svg-icons/action/home';
import {Link} from 'react-router';

const muiTheme = getMuiTheme({
    palette: {
        accent1Color: deepOrange500
    }
});

export class ItemPage extends React.Component {
    constructor(props, context) {
        super(props, context);

        this.state = {
            item: props.item
        };
    }

    render() {

        return (
            <MuiThemeProvider muiTheme={muiTheme}>
                <div>
                <Link to="/">
                    <FloatingActionButton>
                        <HomeLink />
                    </FloatingActionButton>
                </Link>
                    <h1>Read item</h1>
                    <ItemData item={this.state.item}/>
                </div>
            </MuiThemeProvider>
        );
    }
}

function mapStateToProps(state, ownProps) {
    const itemId = ownProps.params.id;
    let item = getItemById(state.items, itemId);
    return {
        item: item
    };
}

function getItemById(items, id) {

    const item = items.find(obj => {return obj.get('id') === id});
    return item || null;
}

ItemPage.contextTypes = {
    router: PropTypes.object
};

ItemPage.propTypes = {
    item: PropTypes.object.isRequired
};

export default connect(mapStateToProps)(ItemPage);