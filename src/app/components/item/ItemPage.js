/**
 * Created by taras on 29.05.16.
 */

import React, {PropTypes} from 'react';
import {connect} from 'react-redux';
import {deepOrange500} from 'material-ui/styles/colors';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import ItemForm from './ItemForm';
import * as itemActions from '../../actions/itemActions';
import FloatingActionButton from 'material-ui/FloatingActionButton';
import HomeLink from 'material-ui/svg-icons/action/home';
import {Link} from 'react-router';

const muiTheme = getMuiTheme({
    palette: {
        accent1Color: deepOrange500
    }
});

const uid = () => Math.random().toString(34).slice(2);

export class ItemPage extends React.Component {
    constructor(props, context) {
        super(props, context);

        this.state = {
            item: props.item
        };

        this.updateItemState = this.updateItemState.bind(this);
    }

    updateItemState(event) {
        const field = event.target.name;
        const value = event.target.value;

        this.props.dispatch(itemActions.updateItemField(this.state.item, {field, value}));
    }

    componentWillReceiveProps(nextProps) {
        if (this.state.item != nextProps.item) {
            this.setState({item: nextProps.item});
        }
    }

    render() {

        return (
            <MuiThemeProvider muiTheme={muiTheme}>
                <div>
                <Link to="/">
                    <FloatingActionButton>
                        <HomeLink />
                    </FloatingActionButton>
                </Link>
                <ItemForm
                    onChange={this.updateItemState}
                    item={this.state.item}
                />
                </div>
            </MuiThemeProvider>
        );
    }
}

function mapStateToProps(state, ownProps) {
    const itemId = ownProps.params.id;

    let item = getItemById(state.items, itemId);

    return {
        item: item
    };
}

function getItemById(items, id) {

    const item = items.find(obj => {return obj.get('id') === id});
    return item || null;
}

ItemPage.contextTypes = {
    router: PropTypes.object
};

ItemPage.propTypes = {
    item: PropTypes.object.isRequired,
    dispatch: PropTypes.func.isRequired
};

export default connect(mapStateToProps)(ItemPage);