/**
 * Created by taras on 29.05.16.
 */

import * as types from '../actions/actionTypes';
import initialState from './initialState';

export default function itemReducer(state = initialState.items, action) {
    switch (action.type) {

        case types.ADD_ITEM:
            return state.push(action.item);

        case types.EDIT_ITEM:
            let item = action.payload.item;
            const data = action.payload.data;

            return state.map(t => {
                    if (t.get('id') === item.get('id')) {
                        return t.set(data.field, data.value);
                    } else {
                        return t;
                    }
            });

        case types.DELETE_ITEM:
            return state.delete(action.index,1);
        
        default:
            return state;
    }
}