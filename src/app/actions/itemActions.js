/**
 * Created by taras on 29.05.16.
 */

import * as types from './actionTypes';

export function addItem(item) {
    return {type: types.ADD_ITEM, item};
}

export function updateItemField(item, data, add) {
    const payload = {item, data, add};
    return {type: types.EDIT_ITEM, payload};
}

export function deleteItem(index) {
    return {type: types.DELETE_ITEM, index};
}