/**
 * Created by taras on 29.05.16.
 */

import {createStore} from 'redux';
import rootReducer from '../reducers';

export default function configureStore(initialState) {
    return createStore(
        rootReducer,
        initialState
    );
}
